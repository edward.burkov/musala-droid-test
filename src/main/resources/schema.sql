CREATE SEQUENCE IF NOT EXISTS DRONE_SEQ START WITH 11;
CREATE SEQUENCE IF NOT EXISTS DRONE_HISTORY_SEQ;
CREATE SEQUENCE IF NOT EXISTS MEDICATION_SEQ;

CREATE TABLE IF NOT EXISTS drone_history (
    history_id bigint PRIMARY KEY,
    drone_id bigint,
    description text NOT NULL,
    created_at timestamp with time zone NOT NULL
);

CREATE TABLE IF NOT EXISTS drone (
    drone_id bigint PRIMARY KEY,
    serial_number VARCHAR(100) NOT NULL,
    model text NOT NULL,
    weight_limit double NOT NULL,
    battery_capacity integer NOT NULL,
    state text NOT NULL
);

CREATE TABLE IF NOT EXISTS medication (
    medication_id bigint PRIMARY KEY,
    name text NOT NULL,
    weight double NOT NULL,
    code text,
    image_data bytea,
    drone_id bigint NOT NULL,
    FOREIGN KEY (drone_id) references drone(drone_id)
);