MERGE INTO drone
    USING VALUES (1, 'LIGHT_WEIGHT', '0001', 200, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (1, 'LIGHT_WEIGHT', '0001', 200, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (2, 'LIGHT_WEIGHT', '0002', 200, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (2, 'LIGHT_WEIGHT', '0002', 200, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (3, 'LIGHT_WEIGHT', '0003', 200, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (3, 'LIGHT_WEIGHT', '0003', 200, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (4, 'LIGHT_WEIGHT', '0004', 200, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (4, 'LIGHT_WEIGHT', '0004', 200, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (5, 'MIDDLE_WEIGHT', '0005', 300, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (5, 'MIDDLE_WEIGHT', '0005', 300, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (6, 'MIDDLE_WEIGHT', '0006', 300, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (6, 'MIDDLE_WEIGHT', '0006', 300, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (7, 'MIDDLE_WEIGHT', '0007', 300, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (7, 'MIDDLE_WEIGHT', '0007', 300, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (8, 'CRUISER_WEIGHT', '0008', 400, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (8, 'CRUISER_WEIGHT', '0008', 400, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (9, 'CRUISER_WEIGHT', '0009', 400, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (9, 'CRUISER_WEIGHT', '0009', 400, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;

MERGE INTO drone
    USING VALUES (10, 'HEAVY_WEIGHT', '0010', 500, 100, 'IDLE') AS t(drone_id, model, serial_number, weight_limit, battery_capacity, state)
        ON drone.drone_id = t.drone_id
    WHEN NOT MATCHED THEN
        INSERT (drone_id, model, serial_number, weight_limit, battery_capacity, state) VALUES (10, 'HEAVY_WEIGHT', '0010', 500, 100, 'IDLE')
    WHEN MATCHED THEN
        UPDATE SET
            model = t.model,
            serial_number = t.serial_number,
            weight_limit = t.weight_limit,
            battery_capacity = t.battery_capacity,
            state = t.state;