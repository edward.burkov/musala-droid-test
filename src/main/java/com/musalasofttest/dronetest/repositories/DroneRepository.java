package com.musalasofttest.dronetest.repositories;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DroneRepository extends CrudRepository<Drone, Long> {
    Optional<Drone> findBySerialNumber(String serialNumber);
    List<Drone> findAllByState(String state);
}
