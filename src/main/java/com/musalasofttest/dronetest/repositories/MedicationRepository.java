package com.musalasofttest.dronetest.repositories;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.Medication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {
}
