package com.musalasofttest.dronetest.repositories;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DroneHistoryRepository extends CrudRepository<DroneHistory, Long> {
    List<DroneHistory> findAllByOrderByCreatedAt();
}
