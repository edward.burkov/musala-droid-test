package com.musalasofttest.dronetest.dto;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneHistory;
import jakarta.persistence.*;

import java.util.Objects;

public class DroneHistoryModel {
    private String droneDescription;
    private String description;

    public DroneHistoryModel(DroneHistory droneHistory) {
        var drone = droneHistory.getDrone();
        if (drone != null) {
            this.droneDescription = "Drone #" + drone.getSerialNumber() +
                ", model: " + drone.getModelType().name();
        } else {
            this.droneDescription = "";
        }
        this.description = droneHistory.getDescription();
    }

    public String getDroneDescription() {
        return droneDescription;
    }

    public void setDroneDescription(String droneDescription) {
        this.droneDescription = droneDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "DroneHistoryModel{" +
            "droneDescription='" + droneDescription + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
