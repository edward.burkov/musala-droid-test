package com.musalasofttest.dronetest.dto;

import com.musalasofttest.dronetest.models.Medication;
import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Arrays;

public class MedicationModel {
    private String name;
    private Double weight;
    private String code;
    private byte[] imageData;

    public MedicationModel() {
    }

    public MedicationModel(Medication medication) {
        this.name = medication.getName();
        this.weight = medication.getWeight();
        this.code = medication.getCode();
        this.imageData = medication.getImageData();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    @Override
    public String toString() {
        return "MedicationModel{" +
            "name='" + name + '\'' +
            ", weight=" + weight +
            ", code='" + code + '\'' +
            ", imageData=" + Arrays.toString(imageData) +
            '}';
    }
}
