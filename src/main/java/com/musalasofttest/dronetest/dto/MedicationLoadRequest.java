package com.musalasofttest.dronetest.dto;

import java.util.List;

public record MedicationLoadRequest(List<MedicationModel> medications) {
}
