package com.musalasofttest.dronetest.dto;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneModelType;
import com.musalasofttest.dronetest.models.DroneState;
import jakarta.persistence.*;

public class DroneModel {
    private String serialNumber;
    private String model;
    private Double weightLimit;
    private Integer batteryCapacity;
    private String state;

    public DroneModel() {
    }

    public DroneModel(Drone drone) {
        this.serialNumber = drone.getSerialNumber();
        this.model = drone.getModelType().name();
        this.weightLimit = drone.getWeightLimit();
        this.batteryCapacity = drone.getBatteryCapacity();
        this.state = drone.getStateType().name();
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Double weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "DroneModel{" +
            ", serialNumber='" + serialNumber + '\'' +
            ", model=" + model +
            ", weightLimit=" + weightLimit +
            ", batteryCapacity=" + batteryCapacity +
            ", state=" + state +
            '}';
    }
}
