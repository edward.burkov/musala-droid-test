package com.musalasofttest.dronetest.services;

import com.musalasofttest.dronetest.dto.DroneHistoryModel;
import com.musalasofttest.dronetest.models.DroneHistory;
import com.musalasofttest.dronetest.repositories.DroneHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DroneHistoryService {
    private final DroneHistoryRepository droneHistoryRepository;

    @Autowired
    public DroneHistoryService(DroneHistoryRepository droneHistoryRepository) {
        this.droneHistoryRepository = droneHistoryRepository;
    }

    @Transactional(rollbackFor = Throwable.class)
    public List<DroneHistoryModel> getAllHistory() {
        return droneHistoryRepository.findAllByOrderByCreatedAt().stream()
            .map(DroneHistoryModel::new)
            .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Throwable.class)
    public void addInfo(DroneHistory droneHistory) {
        droneHistoryRepository.save(droneHistory);
    }
}
