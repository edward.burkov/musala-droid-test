package com.musalasofttest.dronetest.services;

import com.musalasofttest.dronetest.dto.MedicationModel;
import com.musalasofttest.dronetest.exceptions.ValidationException;
import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneState;
import com.musalasofttest.dronetest.models.Medication;
import com.musalasofttest.dronetest.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

@Service
public class MedicationService {
    private static final Pattern medicationNamePattern = Pattern.compile("^([\\w\\-_])+$");
    private static final Pattern medicationCodePattern = Pattern.compile("^([A-Z0-9_])+$");

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public boolean isInvalid(Medication medication) {
        return !medicationNamePattern.matcher(medication.getName()).matches() ||
            !medicationCodePattern.matcher(medication.getCode()).matches();
    }

    public boolean isInvalid(Drone drone, Iterable<Medication> medications) {
        double totalWeight = 0.0;
        for (var item : medications) {
            totalWeight += item.getWeight();
        }
        return drone.getWeightLimit() < totalWeight;
    }

    @Transactional(rollbackFor = Throwable.class)
    public Set<Medication> loadAllOnDrone(Drone drone, Iterable<MedicationModel> medicationModels) throws ValidationException {
        Set<Medication> medications = new HashSet<>();
        for (var item : medicationModels) {
            var medication = new Medication(item);
            if (isInvalid(medication)) {
                throw new ValidationException(String.format("Medication %s is invalid.", item.getCode()));
            }

            medication.setDrone(drone);
            medications.add(medication);
        }

        if (isInvalid(drone, medications)) {
            throw new ValidationException("Drone is not valid for selected cargo.");
        }
        medicationRepository.saveAll(medications);

        return medications;
    }
}
