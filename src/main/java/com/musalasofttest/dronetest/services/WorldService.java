package com.musalasofttest.dronetest.services;

import com.musalasofttest.dronetest.models.Drone;
import com.musalasofttest.dronetest.models.DroneHistory;
import com.musalasofttest.dronetest.models.DroneState;
import com.musalasofttest.dronetest.models.Medication;
import com.musalasofttest.dronetest.repositories.DroneHistoryRepository;
import com.musalasofttest.dronetest.repositories.DroneRepository;
import com.musalasofttest.dronetest.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class WorldService {
    private static final Random randomGenerator = new Random();

    private DroneHistoryRepository droneHistoryRepository;
    private DroneRepository droneRepository;
    private MedicationRepository medicationRepository;

    @Autowired
    public WorldService(DroneHistoryRepository droneHistoryRepository, DroneRepository droneRepository, MedicationRepository medicationRepository) {
        this.droneHistoryRepository = droneHistoryRepository;
        this.droneRepository = droneRepository;
        this.medicationRepository = medicationRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public void tick(String message) {
        synchronized (this) {
            List<DroneHistory> history = new ArrayList<>();
            Set<Medication> medications = new HashSet<>();

            history.add(new DroneHistory(message));
            var drones = droneRepository.findAll();
            for (var drone : drones) {
                switch (drone.getStateType()) {
                    case IDLE -> drone.setBatteryCapacity(100);
                    case LOADING -> {
                        drone.setStateType(DroneState.LOADED);
                        history.add(new DroneHistory(drone, String.format("Drone #%s finished loading cargo.", drone.getSerialNumber())));
                    }
                    case LOADED -> {
                        drone.setStateType(DroneState.DELIVERING);
                        history.add(new DroneHistory(drone, String.format("Drone #%s is delivering cargo.", drone.getSerialNumber())));
                    }
                    case DELIVERING -> {
                        drone.setStateType(DroneState.DELIVERED);
                        int delta = randomGenerator.nextInt(drone.getBatteryCapacity());
                        drone.setBatteryCapacity(drone.getBatteryCapacity() - delta);
                        medications.addAll(drone.getMedications());
                        history.add(
                            new DroneHistory(drone, String.format("Drone #%s has delivered cargo. " +
                                "Consumed %s percent of battery during trip.",
                                drone.getSerialNumber(), delta))
                        );
                    }
                    case DELIVERED -> {
                        drone.setStateType(DroneState.RETURNING);
                        history.add(
                            new DroneHistory(drone, String.format("Drone #%s is on it's way home.", drone.getSerialNumber()))
                        );
                    }
                    case RETURNING -> {
                        drone.setStateType(DroneState.IDLE);
                        drone.setBatteryCapacity(100);
                        history.add(
                            new DroneHistory(drone, String.format("Drone #%s has returned home. Battery recharged.", drone.getSerialNumber()))
                        );
                    }
                    default -> {}
                }
            }

            droneRepository.saveAll(drones);
            if (!medications.isEmpty()) {
                medicationRepository.deleteAll(medications);
            }
            droneHistoryRepository.saveAll(history);
        }
    }

    @Scheduled(fixedDelay = 10000)
    public void worldTick() {
        tick("World ticked.");
    }
}
