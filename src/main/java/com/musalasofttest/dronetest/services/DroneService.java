package com.musalasofttest.dronetest.services;

import com.musalasofttest.dronetest.dto.DroneModel;
import com.musalasofttest.dronetest.dto.MedicationModel;
import com.musalasofttest.dronetest.exceptions.ValidationException;
import com.musalasofttest.dronetest.models.*;
import com.musalasofttest.dronetest.repositories.DroneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DroneService {
    private final DroneRepository droneRepository;
    private final MedicationService medicationService;
    private final DroneHistoryService droneHistoryService;

    @Autowired
    public DroneService(DroneRepository droneRepository, MedicationService medicationService,
                        DroneHistoryService droneHistoryService) {
        this.droneRepository = droneRepository;
        this.medicationService = medicationService;
        this.droneHistoryService = droneHistoryService;
    }

    public boolean isInvalid(Drone drone) {
        return drone.getSerialNumber().length() > 100 ||
            (drone.getBatteryCapacity() < 25 && drone.getStateType().equals(DroneState.LOADING));
    }

    @Transactional(rollbackFor = Throwable.class)
    public void addDrone(DroneModel model) throws ValidationException {
        Drone drone = new Drone();

        drone.setModelType(DroneModelType.valueOf(model.getModel()));
        drone.setStateType(DroneState.IDLE);
        drone.setSerialNumber(model.getSerialNumber());
        drone.setWeightLimit(model.getWeightLimit());
        drone.setBatteryCapacity(100);

        if (isInvalid(drone)) {
            throw new ValidationException("Drone is not valid");
        } else if (droneRepository.findBySerialNumber(drone.getSerialNumber()).isPresent()) {
            throw new ValidationException("Drone with this serial number already exists");
        }

        droneRepository.save(drone);
        droneHistoryService.addInfo(new DroneHistory(drone, String.format("New drone %s added.", model.getSerialNumber())));
    }

    @Transactional(rollbackFor = Throwable.class)
    public Drone getDroneById(Long id) {
        return droneRepository.findById(id).orElse(null);
    }

    @Transactional(rollbackFor = Throwable.class)
    public Drone getDroneBySerialNumber(String serialNumber) {
        return droneRepository.findBySerialNumber(serialNumber).orElse(null);
    }

    public List<DroneModel> getAvailableDrones() {
        return droneRepository.findAllByState(DroneState.IDLE.name()).stream()
            .map(DroneModel::new)
            .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = Throwable.class)
    public void loadDrone(Long id, List<MedicationModel> medicationModels) throws ValidationException {
        var drone = droneRepository.findById(id).orElse(null);

        if (drone == null) {
            throw new ValidationException("Drone is not found.");
        }

        var prevState = drone.getStateType();
        if (!prevState.equals(DroneState.IDLE) && !prevState.equals(DroneState.DELIVERED) && !prevState.equals(DroneState.RETURNING)) {
            throw new ValidationException("Drone is occupied.");
        }

        drone.setStateType(DroneState.LOADING);
        if (isInvalid(drone)) {
            throw new ValidationException("Drone is not valid for loading cargo.");
        }
        droneRepository.save(drone);

        var medications = medicationService.loadAllOnDrone(drone, medicationModels);
        if (prevState.equals(DroneState.DELIVERED) || prevState.equals(DroneState.RETURNING)) {
            droneHistoryService.addInfo(
                new DroneHistory(drone, String.format("Drone #%s was requested to transport cargo on the way home.", drone.getSerialNumber()))
            );
        }
        droneHistoryService.addInfo(
            new DroneHistory(drone, String.format("Drone #%s is loaded with %d medication(s).", drone.getSerialNumber(), medications.size()))
        );
    }

    @Transactional(rollbackFor = Throwable.class)
    public List<MedicationModel> getMedications(Long id) throws ValidationException {
        var drone = droneRepository.findById(id).orElse(null);

        if (drone == null) {
            throw new ValidationException("Drone is not found.");
        }

        return drone.getMedications().stream().
            map(MedicationModel::new)
            .collect(Collectors.toList());
    }
}
