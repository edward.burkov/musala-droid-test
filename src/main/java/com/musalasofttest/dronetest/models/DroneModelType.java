package com.musalasofttest.dronetest.models;

public enum DroneModelType {
    LIGHT_WEIGHT, MIDDLE_WEIGHT, CRUISER_WEIGHT, HEAVY_WEIGHT;
}
