package com.musalasofttest.dronetest.models;

import jakarta.persistence.*;

import java.time.OffsetDateTime;
import java.util.Objects;

@Entity
public class DroneHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "history_seq_generator")
    @SequenceGenerator(name = "history_seq_generator", sequenceName = "DRONE_HISTORY_SEQ", allocationSize = 1)
    private Long historyId;

    @ManyToOne
    @JoinColumn(name = "drone_id")
    private Drone drone;

    @Column
    private String description;

    @Column
    private OffsetDateTime createdAt;

    public DroneHistory() {
    }

    public DroneHistory(String description) {
        this.description = description;
        this.createdAt = OffsetDateTime.now();
    }

    public DroneHistory(Drone drone, String description) {
        this(description);
        this.drone = drone;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(OffsetDateTime createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DroneHistory that = (DroneHistory) o;
        return Objects.equals(historyId, that.historyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(historyId);
    }

    @Override
    public String toString() {
        return "DroneHistory{" +
            "historyId=" + historyId +
            ", drone=" + drone +
            ", description='" + description + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }
}
