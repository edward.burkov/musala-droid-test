package com.musalasofttest.dronetest.models;

import com.musalasofttest.dronetest.dto.DroneModel;
import jakarta.persistence.*;

import java.util.Objects;
import java.util.Set;

@Entity
public class Drone {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "drone_seq_generator")
    @SequenceGenerator(name = "drone_seq_generator", sequenceName = "DRONE_SEQ", allocationSize = 1)
    private Long droneId;

    @Column(nullable = false, updatable = false, length = 100)
    private String serialNumber;

    @Column(nullable = false, updatable = false)
    private String model;

    @Column(nullable = false, updatable = false)
    private Double weightLimit; // assume weightLimit is based on drone model

    @Column(nullable = false)
    private Integer batteryCapacity; // percent, real value = bC / 100

    @Column(nullable = false)
    private String state;

    @OneToMany(mappedBy = "drone", orphanRemoval = true)
    private Set<Medication> medications;

    public Drone() {
    }

    public Long getDroneId() {
        return droneId;
    }

    public void setDroneId(Long droneId) {
        this.droneId = droneId;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public DroneModelType getModelType() {
        return DroneModelType.valueOf(model);
    }

    public void setModelType(DroneModelType model) {
        this.model = model.name();
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = DroneModelType.valueOf(model).name();
    }

    public Double getWeightLimit() {
        return weightLimit;
    }

    public void setWeightLimit(Double weightLimit) {
        this.weightLimit = weightLimit;
    }

    public Integer getBatteryCapacity() {
        return batteryCapacity;
    }

    public void setBatteryCapacity(Integer batteryCapacity) {
        this.batteryCapacity = batteryCapacity;
    }

    public Double getBatteryCapacityPercent() {
        return batteryCapacity / 100.0;
    }

    public void setBatteryCapacityPercent(Double batteryCapacityPercent) {
        this.batteryCapacity = batteryCapacityPercent.intValue() * 100;
    }

    public DroneState getStateType() {
        return DroneState.valueOf(state);
    }

    public void setStateType(DroneState state) {
        this.state = state.name();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = DroneState.valueOf(state).name();
    }

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Drone drone = (Drone) o;
        return Objects.equals(droneId, drone.droneId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(droneId);
    }

    @Override
    public String toString() {
        return "Drone{" +
            "id=" + droneId +
            ", serialNumber=" + serialNumber +
            ", model=" + model +
            ", weightLimit=" + weightLimit +
            ", batteryCapacity=" + batteryCapacity +
            ", state=" + state +
            '}';
    }
}
