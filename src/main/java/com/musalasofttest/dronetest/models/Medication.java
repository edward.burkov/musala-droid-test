package com.musalasofttest.dronetest.models;

import com.musalasofttest.dronetest.dto.MedicationModel;
import jakarta.persistence.*;

import java.util.Arrays;
import java.util.Objects;

@Entity
public class Medication {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "medication_seq_generator")
    @SequenceGenerator(name = "medication_seq_generator", sequenceName = "MEDICATION_SEQ", allocationSize = 1)
    private Long medicationId;

    @Column(nullable = false, updatable = false)
    private String name;

    @Column(nullable = false, updatable = false)
    private Double weight;

    @Column(nullable = false, updatable = false)
    private String code;

    @Column
    private byte[] imageData;

    @Column(nullable = false)
    private Boolean isDelivered;

    @ManyToOne
    @JoinColumn(name = "droneId")
    private Drone drone;

    public Medication() {
        this.imageData = null;
        this.isDelivered = false;
    }

    public Medication(MedicationModel model) {
        this.name = model.getName();
        this.weight = model.getWeight();
        this.code = model.getCode();
        this.imageData = model.getImageData();
        this.isDelivered = false;
    }

    public Long getMedicationId() {
        return medicationId;
    }

    public void setMedicationId(Long medicationId) {
        this.medicationId = medicationId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public Boolean getIsDelivered() {
        return isDelivered;
    }

    public void setIsDelivered(Boolean delivered) {
        isDelivered = delivered;
    }

    public Drone getDrone() {
        return drone;
    }

    public void setDrone(Drone drone) {
        this.drone = drone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Medication that = (Medication) o;
        return Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code);
    }

    @Override
    public String toString() {
        return "Medication{" +
            "id=" + medicationId +
            ", name='" + name + '\'' +
            ", weight=" + weight +
            ", code='" + code + '\'' +
            ", imageData=" + Arrays.toString(imageData) +
            ", isDelivered=" + isDelivered +
            '}';
    }
}
