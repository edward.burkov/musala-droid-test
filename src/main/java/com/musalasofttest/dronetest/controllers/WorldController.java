package com.musalasofttest.dronetest.controllers;

import com.musalasofttest.dronetest.dto.DroneHistoryModel;
import com.musalasofttest.dronetest.services.DroneHistoryService;
import com.musalasofttest.dronetest.services.WorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WorldController {
    private final DroneHistoryService droneHistoryService;
    private final WorldService worldService;

    @Autowired
    public WorldController(DroneHistoryService droneHistoryService, WorldService worldService) {
        this.droneHistoryService = droneHistoryService;
        this.worldService = worldService;
    }

    @GetMapping("/history")
    public ResponseEntity<List<DroneHistoryModel>> getHistory() {
        List<DroneHistoryModel> history = List.of();
        try {
            history = droneHistoryService.getAllHistory();
        } catch (Exception ex) {
            return ResponseEntity.internalServerError().body(history);
        }

        return ResponseEntity.ok(history);
    }

    @PutMapping("/tick")
    public void tickWorld() {
        try {
            worldService.tick("User moved world");
        } catch (Exception ex) {
            System.err.printf("Unexpected error during world ticking. Details: %s%n", ex.getMessage());
        }
    }
}
