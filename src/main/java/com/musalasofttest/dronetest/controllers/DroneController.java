package com.musalasofttest.dronetest.controllers;

import com.musalasofttest.dronetest.dto.DroneModel;
import com.musalasofttest.dronetest.dto.MedicationLoadRequest;
import com.musalasofttest.dronetest.dto.MedicationModel;
import com.musalasofttest.dronetest.exceptions.ValidationException;
import com.musalasofttest.dronetest.services.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/drone")
public class DroneController {
    private final DroneService droneService;

    @Autowired
    public DroneController(DroneService droneService) {
        this.droneService = droneService;
    }

    @PostMapping("/")
    public ResponseEntity<?> registerDrone(@RequestBody DroneModel model) {
        try {
            droneService.addDrone(model);
        } catch (ValidationException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
        return ResponseEntity.ok("Drone was added to a park of drones.");
    }

    @PostMapping("/{id}/cargo")
    public ResponseEntity<?> loadDrone(@PathVariable("id") Long id, @RequestBody MedicationLoadRequest loadRequest) {
        try {
            droneService.loadDrone(id, loadRequest.medications());
        } catch (ValidationException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }
        return ResponseEntity.ok("Drone was sent with cargo.");
    }

    @GetMapping("/{id}/cargo")
    public ResponseEntity<List<MedicationModel>> getLoadedMedications(@PathVariable("id") Long id) {
        List<MedicationModel> medicationModels;

        try {
            medicationModels = droneService.getMedications(id);
        } catch (ValidationException ex) {
            return ResponseEntity.badRequest().body(List.of());
        }
        return ResponseEntity.ok(medicationModels);
    }

    @GetMapping("/available")
    public ResponseEntity<List<DroneModel>> getAvailableDrones() {
        List<DroneModel> drones;

        try {
            drones = droneService.getAvailableDrones();
        } catch (Exception ex) {
            return ResponseEntity.badRequest().body(List.of());
        }

        return ResponseEntity.ok(drones);
    }

    @GetMapping("/{id}/battery")
    public ResponseEntity<String> getDroneBattery(@PathVariable("id") Long id) {
        String battery;

        try {
            var drone = droneService.getDroneById(id);
            if (drone == null) {
                throw new ValidationException("Drone not found");
            }
            battery = String.format("Battery for the drone #%s is %d percent", drone.getSerialNumber(), drone.getBatteryCapacity());
        } catch (ValidationException ex) {
            return ResponseEntity.badRequest().body(ex.getMessage());
        }

        return ResponseEntity.ok(battery);
    }
}
