## Drones

[[_TOC_]]

---

:scroll: **START**


### Introduction

There is a major new technology that is destined to be a disruptive force in the field of transportation: **the drone**. Just as the mobile phone allowed developing countries to leapfrog older technologies for personal communication, the drone has the potential to leapfrog traditional transportation infrastructure.

Useful drone functions include delivery of small items that are (urgently) needed in locations with difficult access.

---

### Task description

We have a fleet of **10 drones**. A drone is capable of carrying devices, other than cameras, and capable of delivering small loads. For our use case **the load is medications**.

A **Drone** has:
- serial number (100 characters max);
- model (Lightweight, Middleweight, Cruiserweight, Heavyweight);
- weight limit (500gr max);
- battery capacity (percentage);
- state (IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING).

Each **Medication** has: 
- name (allowed only letters, numbers, ‘-‘, ‘_’);
- weight;
- code (allowed only upper case letters, underscore and numbers);
- image (picture of the medication case).

Develop a service via REST API that allows clients to communicate with the drones (i.e. **dispatch controller**). The specific communicaiton with the drone is outside the scope of this task. 

The service should allow:
- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone; 
- checking available drones for loading;
- check drone battery level for a given drone;

> Feel free to make assumptions for the design approach. 

---

### Requirements

While implementing your solution **please take care of the following requirements**: 

#### Functional requirements

- There is no need for UI;
- Prevent the drone from being loaded with more weight that it can carry;
- Prevent the drone from being in LOADING state if the battery level is **below 25%**;
- Introduce a periodic task to check drones battery levels and create history/audit event log for this.

---

#### Non-functional requirements

- Input/output data must be in JSON format;
- Your project must be buildable and runnable;
- Your project must have a README file with build/run/test instructions (use DB that can be run locally, e.g. in-memory, via container);
- Required data must be preloaded in the database.
- JUnit tests are optional but advisable (if you have time);
- Advice: Show us how you work through your commit history.

---

:scroll: **END** 

---
### Assumptions:
1) Medication are not stored as unique entities, but processed as if each of them is unique. This influences database scheme.
2) Although wording of a task ('fleet of **10 drones**') implies this number is constant (in this case it'll be 'fleet **starts at 10 drones**'), function of 'registering a drone' implies ability to add new drone. Will go from 2nd.   
3) For states and properties to function accurately it is needed for system to have 'world clock' in order to advance time and change states of drones and cargo. I.e. it is a simulation.
4) Drone can go to LOADING state from any non-delivery state, i.e. RETURNING, DELIVERED.

### Drone API
- "/drone", POST: register new drone. If it is not a valid drone - error.
- "/drone/{id}/cargo", POST: load a drone with medications (supplied in body). If it is too heavy/not free - error. 
- "/drone/{id}/cargo", GET: get a loaded medications on the drone
- "/drone/available", GET: get available drones for loading
- "/drone/{id}/battery", GET: get battery life of a drone

### World API
- "/history", GET: get all history, sorted by 'date'
- "/tick", PUT: tick time of world

### Build&Run
- Build:
1) mvnw.cmd clean install
- Run:
1) mvnw.cmd spring-boot:run
2) %JAVA_HOME%\java.exe -jar ".\target\drone-test-0.0.1-SNAPSHOT.jar"

